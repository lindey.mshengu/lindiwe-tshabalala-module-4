import 'package:flutter/material.dart';
import 'screens/login.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';

void main() => runApp(const MyApp());

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      splash: Column(
        children: [
          Image.asset('lib/assets/splash.jpg')
        ],
      ),
      splashIconSize: 600,
      duration: 3000,
      backgroundColor: Color.fromARGB(255, 2, 31, 46),
      nextScreen: const LoginScreen(),
    );
  }
}
 
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
 
  
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Flutter App',
      home: SplashScreen(),
    );
  }
}