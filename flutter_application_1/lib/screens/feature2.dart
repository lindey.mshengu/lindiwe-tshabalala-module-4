import 'package:flutter/material.dart';
import 'edit_profile.dart';

class Feature2 extends StatelessWidget {
  const Feature2({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Feature2'),
      ),
       floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton:  FloatingActionButton(
        onPressed: () => {
          Navigator.push(context,
          MaterialPageRoute(builder: (context) => const EditProfile()))
        }),

    );
  }
}