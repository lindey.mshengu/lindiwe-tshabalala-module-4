import 'package:flutter/material.dart';
import 'feature2.dart';

class Feature1 extends StatelessWidget {
  const Feature1({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Feature1'),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton:  FloatingActionButton(
        onPressed: () => {
          Navigator.push(context,
          MaterialPageRoute(builder: (context) => const Feature2()))
        }),

    );
  }
}