import 'package:flutter/material.dart';
import 'feature1.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Dashboard'),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton:  FloatingActionButton(
        onPressed: () => {
          Navigator.push(context,
          MaterialPageRoute(builder: (context) => const Feature1()))
        }),

    );
  }
}